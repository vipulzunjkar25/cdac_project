#include<stdio.h>
#include<stdlib.h>
typedef struct node
{
	int data;
	struct node * next;	
}node_t;

node_t *head=NULL;

node_t* add_node(int);
void add_node_last(int);
void display();
void reverse_print(node_t*);
void reverse_list();

int main(void)
{int choice,data;
	while(1)
	{
		printf("1.add\n2.display\n3.reverse_print\n4.reverse_list\n5.exit\n");
		printf("enter choice :");
		scanf("%d",&choice);
		switch(choice)
		{
			case 1:
				printf("Enter data :");
				scanf("%d",&data);
				add_node_last(data);
				break;
			case 2:
				display();
				break;
			case 3:
				reverse_print(head);
				printf("\n");
				break;
			case 4:
				reverse_list();
				printf("done!\n");
				break;
			case 5:
				return 0;
		}	
	}

return 0;
}

void reverse_print(node_t * node)
{	
	if(node == NULL)
		return ;
	reverse_print(node->next);
	printf("%d-> ",node->data);
}

void reverse_list()
{
	node_t *t1 = NULL;
	node_t *t2 = NULL;
	node_t *t3 = NULL;
	t1 = head;
	t2 = head->next;
	t1->next = NULL; 
	while(t2 != NULL)
	{	
		t3 = t2->next;
		t2->next = t1;
		t1 = t2;
		t2 = t3;
	}	
	head = t1;	
}

void add_node_last(int data)
{
	node_t* new = NULL;
	node_t* trav = NULL;
	new = add_node(data);
	if(head == NULL)
	{
		head = new;
	}
	else
	{
		trav = head;
		while(trav->next != NULL)
		{
			trav = trav->next;	
		}
		trav->next = new;
	}	
}

node_t* add_node(int data)
{
	node_t *temp = NULL;
	temp = (node_t*)malloc(sizeof(node_t));
	temp->data = data;
	temp->next = NULL;
	return temp;
}

void display()
{	
	node_t* trav = NULL;
	trav = head;
	while(trav != NULL)
	{
		printf("%d-> ",trav->data);
		trav = trav->next;	
	}	
printf("\n");
}
