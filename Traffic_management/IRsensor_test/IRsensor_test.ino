#define IR 2
#define LED 3

void setup() {
  pinMode(IR,INPUT);
  pinMode(LED,OUTPUT);
}

void loop() {
  int result = digitalRead(IR);
  digitalWrite(LED,result);
}
