package com.example.myapplication;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import android.os.Handler;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    String global_url ="http://192.168.43.247:4000/";
    Switch switchMotor,switchAuto;
    TextView TextBaseTank,TextRoofTank;
    String Base_str = "base_level";
    String Roof_str = "roof_level";
    String Water_str = "water_level";
    int data = 0;
    int base_flag = 1;
    int roof_flag = 1;

    private final Handler handler_base = new Handler();
    private final Handler handler_roof = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextBaseTank = findViewById(R.id.TextBaseTank);
        TextRoofTank = findViewById(R.id.TextRoofTank);

        switchMotor = findViewById(R.id.switchMotor);
        switchMotor.setOnCheckedChangeListener(this);

        switchAuto = findViewById(R.id.switchAuto);
        switchAuto.setOnCheckedChangeListener(this);


    doTheAutoRefresh();
    }

    private void doTheAutoRefresh()
    {
        handler_base.postDelayed(new Runnable() {
            @Override
            public void run() {
               // while(base_flag == 0);
                //base_flag = 0;
                callApiTimed(Water_str);

                /*while(roof_flag == 0);
                roof_flag = 0;
                callApiTimed(Roof_str,Roof_str);
*/
                doTheAutoRefresh();
            }
        },2000);
    }

    /*private void doTheAutoRefresh_base()
    {
        handler_base.postDelayed(new Runnable() {
            @Override
            public void run() {
                while(base_flag == 0);
                    callApiTimed(Base_str, Base_str);
                    doTheAutoRefresh_base();
                    base_flag = 0;
                }
        },2000);
    }

    private void doTheAutoRefresh_roof()
    {
        handler_roof.postDelayed(new Runnable() {
            @Override
            public void run() {
                while(roof_flag == 0);
                    callApiTimed(Roof_str, Roof_str);
                    doTheAutoRefresh_roof();
                    roof_flag = 0;
                }
        },2000);
    }*/

    private void callAPI(String path)
    {
        String url = global_url + path;

        Log.e("MainActivity", "Calling REST API" + url);

        Ion.with(this)
                .load(url)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        Log.e("MainActivity", result);
                    }
                });
    }

   /* public void loop(String data)
    {
        TextRoofTank.setText(data);
        TextBaseTank.setText(data);
    }*/

    private void callApiTimed(String path)
    {
        String url = global_url + path;

        Log.e("MainActivity", "Calling REST API" + url);

        Ion.with(this)
                .load(url)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        if (result != null) {
                            JsonObject object = result.get(0).getAsJsonObject();
                            String base = object.get("base_level").getAsString();
                            String roof = object.get("roof_level").getAsString();
                            Log.e("MainActivity", base);
                            Log.e("MainActivity",roof);
                            TextBaseTank.setText(base);
                            TextRoofTank.setText(roof);

                            /*if (topic.compareTo(Base_str) == 0) {
                                base_flag = 1;
                                TextBaseTank.setText(data);
                            } else if (topic.compareTo(Roof_str) == 0) {
                                roof_flag = 1;
                                TextRoofTank.setText(data);
                            }*/
                        }
                    }
                });

    }

    public void onVoiceCommand(View v) {
        Intent intent = new Intent();
        intent.setAction(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Your command");
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            ArrayList<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (results.size() > 0) {
                String result = results.get(0);
                Log.e("MainActivity", "command: " + result);
                if (result.contains("on")) {
                    if(result.contains("motor")) {
                        switchMotor.setChecked(true);
                        callAPI("motor_on");
                    }
                    else if(result.contains("auto"))
                    {
                        switchAuto.setChecked(true);
                        callAPI("bore_on");
                    }
                } else if (result.contains("off") || result.contains("of")) {
                    if(result.contains("motor")) {
                        switchMotor.setChecked(false);
                        callAPI("motor_off");
                    }else if(result.contains("auto"))
                    {
                        switchAuto.setChecked(false);
                        callAPI("bore_off");
                    }
                }
            }
        }
    }




    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean b) {
        String path ="";
        if(buttonView == switchMotor)
        {
            if(switchMotor.isChecked())
            {
                path = "motor_on";
            }else
            {
                path = "motor_off";
            }
        }
        else if(buttonView == switchAuto)
        {
            if(switchAuto.isChecked())
            {
                path = "bore_on";
            }
            else
            {
                path = "bore_off";
            }
        }
        callAPI(path);

    }
}
