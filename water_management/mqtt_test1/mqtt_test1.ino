#include<ESP8266WiFi.h>
#include<PubSubClient.h>

#define MAX 50

WiFiClient espClient;
PubSubClient client(espClient);

const int trig_p=16;
const int echo_p=5;

long duration;
int distance;
int depth;
float percentage;


const char*ssid = "NFS";
const char*password = "vipul123#";

void on_message(char*topic,byte*payload,unsigned int length)
{
  Serial.print("Received a message on topic = ");
  Serial.println(topic);  
}
void setup() {
Serial.begin(115200);
Serial.flush();
pinMode(A0,INPUT);
pinMode(trig_p,OUTPUT);
pinMode(echo_p,INPUT);
WiFi.mode(WIFI_STA);
WiFi.begin(ssid,password);
Serial.println("Initializing module ...");
delay(500);
Serial.print("Connecting");
while(WiFi.status()!= WL_CONNECTED)
{
  Serial.print(".");
  delay(500);
}
Serial.println();
Serial.println("Connected!");
Serial.print("IP Address:");
Serial.println(WiFi.localIP());

client.setServer("192.168.43.247",1883);
client.setCallback(on_message);

for(int i=0;i<MAX;i++)
  {
  digitalWrite(trig_p,LOW);
  delayMicroseconds(2);
  digitalWrite(trig_p,HIGH);
  delayMicroseconds(10);
  digitalWrite(trig_p,LOW);

  duration = pulseIn(echo_p,HIGH);
  depth = duration*0.034/2;
  Serial.print("Depth :");
  Serial.println(depth);
  delay(100);
  }  
  Serial.println("Reading Started!");
  
}

void loop() {
while(!client.connected())
{
 if (client.connect("NodeMCU"))
 {
  client.publish("test","about to send percentage");
  
 }
 else
 {
  Serial.print(".");
  delay(500);
 } 
 }
 Serial.println();
  Serial.println("Connected to the broker");


 for(int i=0;i<MAX;i++)
  {
  digitalWrite(trig_p,LOW);
  delayMicroseconds(2);
  digitalWrite(trig_p,HIGH);
  delayMicroseconds(10);
  digitalWrite(trig_p,LOW);

  duration = pulseIn(echo_p,HIGH);
  delay(20);
  }
  distance = duration*0.034/2;
  percentage = ((float)distance/depth)*100;
  percentage = 100 - percentage;
  Serial.print("Percentage :");
  Serial.println(percentage);
  char message[10];
  sprintf(message,"%f",percentage);
  client.publish("test",message);
  delay(200);
//boolean ret = client.subscribe("command");
}

