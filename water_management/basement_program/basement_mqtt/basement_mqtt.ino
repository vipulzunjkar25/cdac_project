#include<ESP8266WiFi.h>
#include<PubSubClient.h>

#define MOTOR_ON "MOTOR_ON"
#define MOTOR_OFF "MOTOR_OFF"
#define MAX 50
#define LOWEST_LEVEL 15
#define HIGHEST_LEVEL 85
#define LOWEST_TANK_LEVEL 30
#define HIGHEST_TANK_LEVEL 85

#define SERVER "192.168.43.247"
#define MOTOR 4  //D3
#define BORE 0   //D4
#define SUPPLY 2 //D5


WiFiClient espClient;
PubSubClient client(espClient);

byte level; 
  
const byte trig_p=16; //D0
const byte echo_p=5;  //D1
short bore = 0;     
short motor = 0;
short supply = 0;

long duration;
int distance;
int depth;
int percentage;


const char*ssid = "NFS";
const char*password = "vipul123#";

void on_message(char*topic,byte*payload,unsigned int length)
{ 
  String buff;
    for(int i=0;i<2;i++)
      buff += (char)payload[i];
      buff+='\0';

    if(!strcmp(topic,"roof_level"))
   {
    level = (byte)buff.toInt();
    Serial.println(level);
    Serial.println("Received data from roof node :"); 
  //Serial.println(str);
}
else if(!(strcmp(topic,"bore")))
{
  if(!buff.compareTo("ON"))
  {
    bore = 1;
    digitalWrite(BORE,HIGH);
  }
  else if(!buff.compareTo("OF"))
  {
    bore = 0;
    digitalWrite(BORE,LOW);
  }
  
  Serial.println("Received command to on bore  ");
  Serial.println(buff);
}
else if(!(strcmp(topic,"supply")))
{
   if(!buff.compareTo("ON"))
  {
    supply = 1;
    digitalWrite(SUPPLY,HIGH);
  }
  else if(!buff.compareTo("OF"))
  {
    supply = 0;
    digitalWrite(SUPPLY,LOW);
  }
}
else if(!(strcmp(topic,"motor")))
{
  if(!buff.compareTo("ON"))
  {
    motor = 1;
    digitalWrite(MOTOR,HIGH);
  }
  else if(!buff.compareTo("OF"))
  {
    motor = 0;
    digitalWrite(MOTOR,LOW);
  }
  
}
}


void setup() {
Serial.begin(115200);
Serial.flush();
pinMode(trig_p,OUTPUT);
pinMode(BORE,OUTPUT);
pinMode(MOTOR,OUTPUT);
pinMode(echo_p,INPUT);
pinMode(SUPPLY,OUTPUT);

digitalWrite(SUPPLY,LOW);
digitalWrite(MOTOR,LOW);
digitalWrite(BORE,LOW);

WiFi.mode(WIFI_STA);
WiFi.begin(ssid,password);
Serial.println("Initializing module ...");
delay(500);
Serial.print("Connecting");
while(WiFi.status()!= WL_CONNECTED)
{
  Serial.print(".");
  delay(500);
}
Serial.println();
Serial.println("Connected!");
Serial.print("IP Address:");
Serial.println(WiFi.localIP());

client.setServer(SERVER,1883);
client.setCallback(on_message);

for(int i=0;i<MAX;i++)
  {
  digitalWrite(trig_p,LOW);
  delayMicroseconds(2);
  digitalWrite(trig_p,HIGH);
  delayMicroseconds(10);
  digitalWrite(trig_p,LOW);

  duration = pulseIn(echo_p,HIGH);
  depth = duration*0.034/2;
  Serial.print("Depth :");
  Serial.println(depth);
  delay(100);
  }  
  Serial.println("Reading Started!");
  
}

void loop() {
while(!client.connected())
{
 if (client.connect("NodeMCU_base"))
 {  
  while(!(client.subscribe("roof_level") && client.subscribe("bore") && client.subscribe("supply") && client.subscribe("motor"))); 
 }
 else
 {
  Serial.print(".");
  delay(500);
 } 
 }
 //Serial.println();
  //Serial.println("Connected to the broker");

 for(int i=0;i<MAX;i++)
  {
  digitalWrite(trig_p,LOW);
  delayMicroseconds(2);
  digitalWrite(trig_p,HIGH);
  delayMicroseconds(10);
  digitalWrite(trig_p,LOW);

  duration = pulseIn(echo_p,HIGH);
  delay(20);
  }
  distance = duration*0.034/2;
  percentage =100 - ((float)distance/depth)*100;
  //Serial.print("Percentage :");
  //Serial.println(percentage);
  char message[5];
  sprintf(message,"%d",percentage);
 
  client.loop();
  client.publish("base_level",message);

  
  if((bore == 1) || (supply == 1))
  {
  if((percentage > HIGHEST_LEVEL) || (supply == 1))
  {     if(bore == 1)
      {
        bore = 0;
        digitalWrite(BORE,LOW);   
      }
      if(percentage > HIGHEST_LEVEL && level > HIGHEST_TANK_LEVEL)
      {
        supply = 0;
        digitalWrite(SUPPLY,LOW); 
      }
  }
   if(percentage > LOWEST_LEVEL)
    {
      if(motor !=1 && level <= HIGHEST_TANK_LEVEL)
      {
        motor = 1;
      digitalWrite(MOTOR,HIGH);
      }
      else if(level > HIGHEST_TANK_LEVEL)
      {
        motor = 0;
        digitalWrite(MOTOR,LOW);
      }
    }
    else if(percentage <= LOWEST_LEVEL && motor == 1)
    {
      motor = 0;
      digitalWrite(MOTOR,LOW);
    }
  }
  else if(level< LOWEST_TANK_LEVEL && percentage > LOWEST_LEVEL)
  {
    if(motor !=1) 
    {
      motor =1;
      digitalWrite(MOTOR,HIGH);
    }
  }
  else if(percentage <= LOWEST_LEVEL || level > HIGHEST_TANK_LEVEL)
  {
    if(motor == 1)
    {
      motor = 0;
     digitalWrite(MOTOR,LOW);
    }  
  }
  delay(200);
}

