#include <Servo.h>

Servo myservo;  /* create servo object to control a servo */

  /* analog pin used to connect the potentiometer */
int val;    /*  variable to read the value from the analog pin */

void setup() {
  Serial.begin(9600);
  myservo.attach(2);  /* attaches the servo on pin 9 to the servo object */
}

void loop() {

  while(Serial.available() <= 0);
   val = (int)Serial.parseInt();
  Serial.print("entered Value : ");
  Serial.println(val);
 // val = map(val, 0, 1023, 0, 255);     /* scale it to use it with the servo (value between 0 and 180) */
  //Serial.print("Mapped Value : ");
  //Serial.print(val);
  //Serial.print("\n\n");
  myservo.write(val);                  /* sets the servo position according to the scaled value */
  delay(1000);                         /* waits for the servo to get there */
}
