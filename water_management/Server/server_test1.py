from flask import Flask, jsonify, request
import paho.mqtt.client as mqtt
app = Flask(__name__)
client = mqtt.Client()
semaphore = 1;
base_level=0
roof_level=0

roof_flag=0
base_flag=0

def on_connect(client,userdata,flags,rc):
    print("connected")
    client.subscribe("roof_level")
    client.subscribe("base_level")

def on_message(client,userdata,msg):
    print(f"message received :{msg.topic}",str(msg.payload.decode("utf-8")))
    if(msg.topic == "base_level"):
        global base_level
        global base_flag
        base_flag = 1
        base_level_str = msg.payload.decode("utf-8")
        base_level = int(base_level_str)

    elif(msg.topic == "roof_level"):
        global roof_level
        global roof_flag
        roof_flag = 1
        roof_level_str = msg.payload.decode("utf-8")
        roof_level = int(roof_level_str)
        print(roof_level)


client.on_connect = on_connect
client.on_message = on_message
client.connect("192.168.43.247", 1883, 60)

@app.route("/",methods=["GET","POST"])
def welcome():
    return "Welcome to Flask server"

@app.route("/motor_on",methods=["GET"])
def motor_on():
    client.publish("motor",payload="ON")
    return "motor getting on"
@app.route("/motor_off",methods=["GET"])
def motor_off():
    client.publish("motor",payload="OFF")
    return "motor getting off"

@app.route("/bore_on",methods=["GET"])
def bore_on():
    client.publish("bore", payload="ON")
    return "bore getting on"
@app.route("/bore_off",methods=["GET"])
def bore_off():
    client.publish("bore",payload="OFF")
    return "bore getting off"

@app.route("/roof_level",methods=["GET"])
def get_roof_level():
    global roof_flag
    global roof_level
    global semaphore

    while(semaphore == 0):
        no = 0
    client.loop_start()
    while(roof_flag == 0):
        no_op=0
    roof_flag = 0
    client.loop_stop()
    semaphore = 1
    roof_data = []
    roof_data.append({"roof_level":roof_level})
    return jsonify(roof_data)

@app.route("/base_level",methods=["GET"])
def get_base_level():
    global base_flag
    global base_level
    global semaphore
    while(semaphore == 0):
        no = 0
    client.loop_start()
    while(base_flag == 0):
        no_op=0
    base_flag = 0
    client.loop_stop()
    semaphore = 1
    base_data = []
    base_data.append({"base_level":base_level})
    return jsonify(base_data)

app.run(host="192.168.43.247", port=4000, debug=True)



