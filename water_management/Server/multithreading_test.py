from flask import Flask, jsonify, request
import paho.mqtt.client as mqtt
import threading
app = Flask(__name__)
client = mqtt.Client()

roof_level = 25
base_level = 0


def on_connect(client,userdata,flags,rc):
    print("connected")
    client.subscribe("roof_level")
    client.subscribe("base_level")


def on_message(client,userdata,msg):
    print(f"message received :{msg.topic}",str(msg.payload.decode("utf-8")))
    if(msg.topic == "base_level"):
        base_level = msg.payload.decode("utf-8")
    elif(msg.topic == "roof_level"):
        roof_level = msg.payload.decode("utf-8")


client.on_connect = on_connect
client.on_message = on_message
client.connect("192.168.1.217", 1883, 60)

@app.route("/",methods=["GET","POST"])
def welcome():
    return "Welcome to Flask server"

@app.route("/motor",methods=["GET","POST"])
def motor_on():
    motor = request.json.get("motor")
    if(motor == "ON"):
        client.publish("motor",payload="ON")
        return "motor getting on"
    elif(motor == "OFF"):
        client.publish("motor",payload="OFF")
        return "motor getting off"

@app.route("/bore",methods=["GET","POST"])
def motor_off():
    bore = request.json.get("bore")
    if(bore == "ON"):
        client.publish("bore", payload="ON")
        return "bore getting on"
    elif(bore == "OFF"):
        client.publish("bore", payload="OFF")
        return "bore getting off"

@app.route("/roof",methods=["GET"])
def get_roof_level():
    roof_data = []
    roof_data.append({"roof_level":roof_level})
    return jsonify(roof_data)


app.run(host="192.168.1.217", port=4000, debug=True)



