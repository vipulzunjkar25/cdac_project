import threading

def func1():
    for index in range(1,6):
        print("hello there! this is one thread!")
    def func3():
        print("hiiii")
    func3()

def func2():
    for index in range(1,6):
        print("Hello this is another thread!")

t1 = threading.Thread(target=func1)
t2 = threading.Thread(target=func2)

t1.start()
t2.start()

for index in range(1,6):
    print("this is main")