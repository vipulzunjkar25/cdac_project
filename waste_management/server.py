from flask import Flask , jsonify
import paho.mqtt.client as mqtt
app = Flask(__name__)
client = mqtt.Client()

total_count_dustbin = 1
count = total_count_dustbin

data_list = []

def on_connect(client,userdata,flags,rc):
    client.subscribe("phase-1")
    print("connected")

def on_message(client,userdata,msg):
    global data_list
    global count
    count = count -1
    data_list.append({"area":msg.topic, "value":msg.payload.decode("utf-8")})
    print(data_list)
client.on_connect = on_connect
client.on_message = on_message
client.connect("192.168.43.247", 1883, 60)

@app.route("/get_data",methods=["GET"])
def get_data():
    global data_list
    global count
    global total_count_dustbin

    data_list.clear()
    client.loop_start()
    while(count != 0):
        no_op = 0
    count = total_count_dustbin
    client.loop_stop()
    return jsonify(data_list)

app.run("192.168.43.247",port=4000,debug=True)
