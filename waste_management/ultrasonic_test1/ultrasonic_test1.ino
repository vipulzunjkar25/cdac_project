#include<ESP8266WiFi.h>
#include<PubSubClient.h>
#include<Servo.h>

#define INIT_ANGLE 50
#define LOWER_ANGLE 60
#define UPPER_ANGLE 120
#define TOTAL_PARTITION 12 
#define SERVO_PIN D0
#define MAX 10
Servo servo;
#define trig_p D1
#define echo_p D2
#define SERVER "192.168.43.247"

char* ssid = "NFS";
char*password = "vipul123#";

WiFiClient espClient;
PubSubClient client(espClient);
char payload[10];
const int calibration[] = {18,17,17,17,16,16,16,16,16,17,17,17,18};
long duration;
int distance;
int depth;
int percentage;
int avg_percentage;
int depth_arr[TOTAL_PARTITION + 1];


void setup() {

pinMode(trig_p,OUTPUT);
pinMode(echo_p,INPUT);
Serial.begin(115200);
Serial.flush();
servo.attach(SERVO_PIN);
servo.write(INIT_ANGLE);
WiFi.mode(WIFI_STA);
WiFi.begin(ssid,password);
Serial.println("Initializing module...");
delay(500);
Serial.print("Connecting..");
while(WiFi.status()!=WL_CONNECTED)
{
  Serial.print(".");
  delay(500);
}
Serial.println();
Serial.println("Connected!");
Serial.print("IP Address:");
Serial.println(WiFi.localIP());

client.setServer(SERVER,1883);
delay(1000);

int arr[MAX];
for(int pos = LOWER_ANGLE ; pos<=UPPER_ANGLE ;pos+=5)
{
  servo.write(pos);
  delay(150);
  long sum = 0;
for( int i = 0; i<MAX ; i++ )
{
    digitalWrite(trig_p,LOW);
    delayMicroseconds(2);
    digitalWrite(trig_p,HIGH);
    delayMicroseconds(10);
    digitalWrite(trig_p,LOW);    

    duration = pulseIn(echo_p,HIGH);
    depth = duration*0.034/2;
    sum +=depth;
}
distance = (sum/MAX) - calibration[(pos - LOWER_ANGLE)/5];

Serial.print("Distance : ");
Serial.println(distance);
depth_arr[(pos-LOWER_ANGLE)/5] = distance;
delay(50);
}
}


void loop() {
int arr[MAX];
avg_percentage = 0;

while(!client.connected())
{
 if (client.connect("NodeMCU"))
 {
  sprintf(payload,"%d",avg_percentage);   
  client.publish("Phase-1",payload);
 }
 else
 {
  Serial.print(".");
  delay(500);
 } 
 }

for(int pos =LOWER_ANGLE; pos<=UPPER_ANGLE;pos+=5)
{
  servo.write(pos);
  delay(150);
  long sum = 0;
for( int i = 0; i<MAX ; i++ )
{
    digitalWrite(trig_p,LOW);
    delayMicroseconds(2);
    digitalWrite(trig_p,HIGH);
    delayMicroseconds(10);
    digitalWrite(trig_p,LOW);    

    duration = pulseIn(echo_p,HIGH);
    depth = duration*0.034/2;
    sum  += depth;
}
distance = sum/MAX - calibration[(pos - LOWER_ANGLE)/5];
percentage = 100 - ((float)distance/depth_arr[(pos - LOWER_ANGLE)/5] * 100);
avg_percentage += percentage;
Serial.print("Percentage : ");
Serial.println(percentage);
delay(50);
}
avg_percentage = avg_percentage/(TOTAL_PARTITION + 1);
Serial.println("");
Serial.print("Avg percentage :");
Serial.println(avg_percentage);
sprintf(payload,"%d",avg_percentage);
client.publish("phase-1",payload);
delay(1000);

avg_percentage = 0;
for(int pos = UPPER_ANGLE ; pos >=LOWER_ANGLE ; pos-=5)
{
  servo.write(pos);
  delay(150);
  long sum = 0;
for( int i = 0; i<MAX ; i++ )
{
    digitalWrite(trig_p,LOW);
    delayMicroseconds(2);
    digitalWrite(trig_p,HIGH);
    delayMicroseconds(10);
    digitalWrite(trig_p,LOW);    

    duration = pulseIn(echo_p,HIGH);
    depth = duration*0.034/2;
    sum += depth;
}
distance = sum/MAX - calibration[(pos - LOWER_ANGLE)/5];
percentage = 100 - ((float)distance/depth_arr[(pos - LOWER_ANGLE)/5] * 100);
avg_percentage += percentage;
Serial.print("Percentage : ");
Serial.println(percentage);
delay(50);
}
avg_percentage = avg_percentage/(TOTAL_PARTITION + 1);
Serial.println("");
Serial.print("Avg percentage :");
Serial.println(avg_percentage);
sprintf(payload,"%d",avg_percentage);
client.publish("phase-1",payload);
delay(1000);
}

