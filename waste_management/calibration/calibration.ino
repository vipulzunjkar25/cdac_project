#include<ESP8266WiFi.h>
#include<Servo.h>

#define INIT_ANGLE 90
#define SERVO_PIN D0
#define MAX 10
Servo servo;
#define trig_p D1
#define echo_p D2

long duration;
int distance;
int depth;
int percentage;

int depth_arr[17];
void setup() {

pinMode(trig_p,OUTPUT);
pinMode(echo_p,INPUT);
Serial.begin(115200);
Serial.flush();
servo.attach(SERVO_PIN);
servo.write(INIT_ANGLE);
delay(3000);
}

void loop() {
int arr[MAX];
for(int pos = 60 ; pos<=120;pos+=5)
{
  servo.write(pos);
  delay(150);
  long sum = 0;
for( int i = 0; i<MAX ; i++ )
{
    digitalWrite(trig_p,LOW);
    delayMicroseconds(2);
    digitalWrite(trig_p,HIGH);
    delayMicroseconds(10);
    digitalWrite(trig_p,LOW);    

    duration = pulseIn(echo_p,HIGH);
    depth = duration*0.034/2;
    sum  += depth;
}
distance = sum/MAX;
Serial.print("Distance : ");
Serial.println(distance);
delay(50);
}

for(int pos = 120 ; pos >=60 ; pos-=5)
{
  servo.write(pos);
  delay(150);
  long sum =0;
for( int i = 0; i<MAX ; i++ )
{
    digitalWrite(trig_p,LOW);
    delayMicroseconds(2);
    digitalWrite(trig_p,HIGH);
    delayMicroseconds(10);
    digitalWrite(trig_p,LOW);    

    duration = pulseIn(echo_p,HIGH);
    depth = duration*0.034/2;
    sum += depth;
}
distance = sum/MAX;
Serial.print("Distance : ");
Serial.println(distance);
delay(50);
}

}
